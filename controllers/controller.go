package controllers

import (
	"log"
	"net/http"
	"pixel-tracking/models"

	"github.com/gin-gonic/gin"
)

func TrackLeadEvent(c *gin.Context) {
	var leadEvent models.CaptureLeadEvent
	err := c.ShouldBindJSON(&leadEvent)
	if err != nil {
		//handle error
		log.Fatal(err)
	}
	err = models.TrackLeadEvent(leadEvent)
	if err != nil {
		//handle error
		log.Fatal(err)
	}
	c.JSON(http.StatusOK, gin.H{
		"message": "success",
	})
}
