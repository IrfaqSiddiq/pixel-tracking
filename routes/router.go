package routes

import (
	"pixel-tracking/controllers"

	"github.com/gin-gonic/gin"
)

func AddRoutes(router *gin.RouterGroup) {
	router.POST("/track-event", controllers.TrackLeadEvent)
}

func SetupRouter() *gin.Engine {

	router := gin.Default()
	AddRoutes(&router.RouterGroup)
	return router
}
