package models

import (
	"log"
	"pixel-tracking/config"
)

type CaptureLeadEvent struct {
	EventName string
	EventData string
	UserHash  string
}

func TrackLeadEvent(leadEvent CaptureLeadEvent) error {
	db, err := config.GetDB2()
	if err != nil {
		log.Println("TrackLeadEvent: Failed while connecting with the database :", err)
		return err
	}
	defer db.Close()
	query := `UPDATE jobs.company2lead SET status = $1 where connection_hash = $2`
	_, err = db.Exec(query, leadEvent.EventName, leadEvent.UserHash)
	if err != nil {
		log.Println("TrackLeadEvent: Failed while executing the query :", err)
		return err
	}
	return nil
}
